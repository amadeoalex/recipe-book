package amadeo.recipebook.utils

import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.Transformation

fun View.applySmoothAnimationChange(child: View) {
    val view = this

    val childHeight = child.measuredHeight
    val currentHeight = this.measuredHeight
    view.layoutParams.height = currentHeight
    view.requestLayout()

    val animation = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            view.layoutParams.height = currentHeight - (childHeight * interpolatedTime).toInt()
            view.requestLayout()
        }

        override fun willChangeBounds(): Boolean = true
    }

    animation.duration = 500
    animation.interpolator = AccelerateInterpolator(2.0f)

    view.startAnimation(animation)
}