package amadeo.recipebook.dao

import amadeo.recipebook.entity.Recipe
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RecipeDao {

    @Insert
    fun insert(recipe: Recipe): Long

    @Insert
    fun insertAll(recipes: List<Recipe>)

    @Delete
    fun delete(recipe: Recipe)

    @Query("DELETE FROM recipes WHERE id == :recipeId")
    fun delete(recipeId: Long)

    @Update
    fun update(recipe: Recipe)

    @Query("SELECT * FROM recipes")
    fun getAll(): LiveData<List<Recipe>>

    @Query("SELECT * FROM recipes WHERE name LIKE '%'||:nameFragment||'%'")
    fun getAllWithName(nameFragment: String): LiveData<List<Recipe>>

    @Query("SELECT * FROM recipes WHERE ID == :recipeId")
    fun getRecipe(recipeId: Long): LiveData<Recipe>
}