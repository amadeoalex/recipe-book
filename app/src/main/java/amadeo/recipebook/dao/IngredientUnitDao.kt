package amadeo.recipebook.dao

import amadeo.recipebook.entity.IngredientUnit
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface IngredientUnitDao {

    @Insert
    fun insert(ingredientUnit: IngredientUnit): Long

    @Insert
    fun insertAll(ingredientUnits: List<IngredientUnit>)

    @Delete
    fun delete(ingredientUnit: IngredientUnit)

    @Update
    fun update(ingredientUnit: IngredientUnit)

    @Query("SELECT * FROM ingredientUnits")
    fun getAll(): LiveData<List<IngredientUnit>>

    @Query("SELECT * FROM ingredientUnits WHERE name LIKE :nameFragment")
    fun getAllWithName(nameFragment: String): LiveData<List<IngredientUnit>>
}