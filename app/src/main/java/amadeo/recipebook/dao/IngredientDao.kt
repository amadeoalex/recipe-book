package amadeo.recipebook.dao

import amadeo.recipebook.entity.Ingredient
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface IngredientDao {

    @Insert
    fun insert(ingredient: Ingredient): Long

    @Insert
    fun insertAll(ingredients: List<Ingredient>)

    @Delete
    fun delete(ingredient: Ingredient)

    @Update
    fun update(ingredient: Ingredient)

    @Query("SELECT * FROM ingredients")
    fun getAll(): LiveData<List<Ingredient>>

    @Query("SELECT * FROM ingredients WHERE name LIKE :nameFragment")
    fun getAllWithName(nameFragment: String): LiveData<List<Ingredient>>
}