package amadeo.recipebook.recipe

import amadeo.recipebook.entity.Ingredient
import amadeo.recipebook.entity.IngredientUnit
import amadeo.recipebook.entity.getLabel
import amadeo.recipebook.entity.isLabel
import kotlinx.serialization.Serializable

@Serializable
data class Component(
    val ingredient: Ingredient,
    val quantity: Float,
    val unit: IngredientUnit
)


private const val LABEL_QUANTITY = 123.0f

fun Component.isLabel(): Boolean {
    return this.quantity == LABEL_QUANTITY &&
            this.unit.isLabel() &&
            this.ingredient.isLabel()


}

fun Component.getLabel(): String {
    return this.ingredient.getLabel()
}