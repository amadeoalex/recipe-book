package amadeo.recipebook.recipe

import kotlinx.serialization.Serializable

@Serializable
data class Step(
    val content: String
)

private const val LABEL_PREFIX = "__LABEL__"

fun Step.isLabel(): Boolean {
    return this.content.startsWith(LABEL_PREFIX)
}

fun Step.getLabel(): String {
    return this.content.removePrefix(LABEL_PREFIX)
}