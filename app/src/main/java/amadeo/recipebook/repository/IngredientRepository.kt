package amadeo.recipebook.repository

import amadeo.recipebook.dao.IngredientDao
import amadeo.recipebook.entity.Ingredient
import androidx.lifecycle.LiveData

class IngredientRepository(private val ingredientDao: IngredientDao) {

    val ingredients: LiveData<List<Ingredient>> = ingredientDao.getAll()

    fun insertAll(ingredients: List<Ingredient>) {
        ingredientDao.insertAll(ingredients)
    }

    fun insert(ingredient: Ingredient): Long {
        return ingredientDao.insert(ingredient)
    }

}