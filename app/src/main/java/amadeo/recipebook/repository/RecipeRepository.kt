package amadeo.recipebook.repository

import amadeo.recipebook.dao.RecipeDao
import amadeo.recipebook.entity.Recipe
import androidx.lifecycle.LiveData

class RecipeRepository(private val recipeDao: RecipeDao) {

    val recipes: LiveData<List<Recipe>> = recipeDao.getAll()

    fun getRecipe(recipeId: Long): LiveData<Recipe> {
        return recipeDao.getRecipe(recipeId)
    }

    fun getAllWithName(name: String): LiveData<List<Recipe>> {
        return recipeDao.getAllWithName(name)
    }

    fun insert(recipe: Recipe) {
        recipeDao.insert(recipe)
    }

    fun insertAll(recipes: List<Recipe>) {
        recipeDao.insertAll(recipes)
    }

    fun delete(recipeId: Long){
        recipeDao.delete(recipeId)
    }

}