package amadeo.recipebook.repository

import amadeo.recipebook.dao.IngredientUnitDao
import amadeo.recipebook.entity.IngredientUnit
import androidx.lifecycle.LiveData

class IngredientUnitRepository(private val ingredientUnitDao: IngredientUnitDao) {

    val ingredientUnits: LiveData<List<IngredientUnit>> = ingredientUnitDao.getAll()

    fun insertAll(ingredientUnits: List<IngredientUnit>) {
        ingredientUnitDao.insertAll(ingredientUnits)
    }

    fun insert(ingredientUnit: IngredientUnit): Long {
        return ingredientUnitDao.insert(ingredientUnit)
    }
}