package amadeo.recipebook.database

import amadeo.recipebook.dao.IngredientDao
import amadeo.recipebook.entity.Ingredient
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Ingredient::class], version = 1)
abstract class IngredientDatabase : RoomDatabase() {
    abstract fun ingredientDao(): IngredientDao

    companion object {
        @Volatile
        private var instance: IngredientDatabase? = null

        fun getDatabase(context: Context): IngredientDatabase {
            val value = instance
            if (value != null)
                return value

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    IngredientDatabase::class.java,
                    "ingredient_database"
                ).build()

                this.instance = instance
                return instance
            }
        }
    }
}