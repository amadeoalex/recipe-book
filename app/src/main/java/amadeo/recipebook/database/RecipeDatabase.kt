package amadeo.recipebook.database

import amadeo.recipebook.dao.RecipeDao
import amadeo.recipebook.entity.Recipe
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Recipe::class], version = 1)
abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao

    companion object {
        @Volatile
        private var instance: RecipeDatabase? = null

        fun getDatabase(context: Context): RecipeDatabase {
            val value = instance
            if (value != null)
                return value

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RecipeDatabase::class.java,
                    "recipe_database"
                ).build()

                this.instance = instance
                return instance
            }
        }
    }
}