package amadeo.recipebook.database

import amadeo.recipebook.dao.IngredientUnitDao
import amadeo.recipebook.entity.IngredientUnit
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [IngredientUnit::class], version = 1)
abstract class IngredientUnitDatabase : RoomDatabase() {
    abstract fun ingredientUnitDao(): IngredientUnitDao

    companion object {
        @Volatile
        private var instance: IngredientUnitDatabase? = null

        fun getDatabase(context: Context): IngredientUnitDatabase {
            val value = instance
            if (value != null)
                return value

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    IngredientUnitDatabase::class.java,
                    "ingredient_unit_database"
                ).build()

                this.instance = instance
                return instance
            }
        }
    }
}