package amadeo.recipebook

import amadeo.recipebook.recipe.Component
import amadeo.recipebook.recipe.Step
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object RoomConverters {

    @TypeConverter
    @JvmStatic
    fun fromListOfComponents(input: List<Component>): String {
        return Gson().toJson(input)
    }

    @TypeConverter
    @JvmStatic
    fun toListOfComponents(input: String): List<Component> {
        return Gson().fromJson(input, object : TypeToken<List<Component>>() {}.type)
    }

    @TypeConverter
    @JvmStatic
    fun fromListOfSteps(input: List<Step>): String {
        return Gson().toJson(input)
    }

    @TypeConverter
    @JvmStatic
    fun toListOfSteps(input: String): List<Step> {
        return Gson().fromJson(input, object : TypeToken<List<Step>>() {}.type)
    }

}