package amadeo.recipebook.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Entity(tableName = "ingredientUnits")
@Serializable
data class IngredientUnit(
    @PrimaryKey(autoGenerate = true)
    var id: Long,

    val name: String
) {
    override fun toString(): String {
        return name
    }
}

private const val LABEL_UNIT_NAME = "__LABEL__"

fun IngredientUnit.isLabel(): Boolean {
    return this.name == LABEL_UNIT_NAME
}