package amadeo.recipebook.entity

import amadeo.recipebook.RoomConverters
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.recipe.Step
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.serialization.Serializable

@Entity(tableName = "recipes")
@TypeConverters(RoomConverters::class)
@Serializable
data class Recipe(
    @PrimaryKey(autoGenerate = true)
    var id: Long,

    val name: String,
    val components: List<Component>,
    val steps: List<Step>
)