package amadeo.recipebook.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Entity(tableName = "ingredients")
@Serializable
data class Ingredient(
    @PrimaryKey(autoGenerate = true)
    var id: Long,

    val name: String
)

private const val LABEL_INGREDIENT_PREFIX = "__LABEL__"

fun Ingredient.isLabel(): Boolean {
    return this.name.startsWith(LABEL_INGREDIENT_PREFIX)
}

fun Ingredient.getLabel(): String {
    return this.name.removePrefix(LABEL_INGREDIENT_PREFIX)
}