package amadeo.recipebook.ui.addrecipe

import amadeo.recipebook.R
import amadeo.recipebook.entity.Ingredient
import amadeo.recipebook.entity.IngredientUnit
import amadeo.recipebook.utils.hideKeyboard
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_component_picker_dialog.view.*

class ComponentPickerDialogFragment : DialogFragment() {

    interface OnComponentPickedListener {
        fun onComponentPicked(ingredient: String, quantity: Float, unit: String)
    }

    var listener: OnComponentPickedListener? = null

    private var dialogView: View? = null

    private lateinit var ingredient: TextInputLayout
    private lateinit var quantity: TextInputLayout
    private lateinit var unit: TextInputLayout

    private lateinit var viewModel: AddRecipeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        return dialogView
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = activity!!.layoutInflater.inflate(R.layout.fragment_component_picker_dialog, null)
        dialogView = view

        ingredient = view.ingredient
        quantity = view.quantity
        unit = view.unit

        val dialog = MaterialAlertDialogBuilder(requireActivity())
            .setTitle(R.string.select_component)
            .setPositiveButton(android.R.string.ok) { _, _ ->}
            .setNegativeButton(R.string.cancel) { _, _ ->
                activity?.hideKeyboard()
            }
            .setView(view)
            .create()

        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                if (validateInputs()) {
                    val ingredientString: String = ingredient.editText?.text.toString()
                    val quantityFloat: Float = quantity.editText?.text.toString().toFloat()
                    val unitString: String = unit.editText?.text.toString()
                    listener?.onComponentPicked(ingredientString, quantityFloat, unitString)
                    activity?.hideKeyboard()
                    dialog.cancel()
                }
            }
        }

        return dialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(requireActivity())[AddRecipeViewModel::class.java]
        viewModel.ingredients.observe(viewLifecycleOwner, Observer<List<Ingredient>> { newIngredients ->
            val ingredientList: List<String> = newIngredients.map { it.name }
            val ingredientAdapter: ArrayAdapter<String> = ArrayAdapter(activity!!, android.R.layout.simple_dropdown_item_1line, ingredientList)
            val ingredientTextView: AutoCompleteTextView = ingredient.editText as AutoCompleteTextView
            ingredientTextView.setAdapter(ingredientAdapter)
        })

        viewModel.ingredientUnits.observe(viewLifecycleOwner, Observer<List<IngredientUnit>> { newUnits ->
            val unitList: List<String> = newUnits.map { it.name }
            val unitAdapter: ArrayAdapter<String> = ArrayAdapter(activity!!, android.R.layout.simple_dropdown_item_1line, unitList)
            val unitTextView: AutoCompleteTextView = unit.editText as AutoCompleteTextView
            unitTextView.setAdapter(unitAdapter)
        })

    }

    private fun validateInputs(): Boolean {
        ingredient.error = ""
        quantity.error = ""
        unit.error = ""

        val ingredientText: String = ingredient.editText?.text.toString()
        if (ingredientText.isBlank())
            ingredient.error = requireContext().getString(R.string.cannot_be_empty)

        val quantityString: String = quantity.editText?.text.toString()
        if (quantityString.isNotBlank()) {
            val parsedQuantity: Float? = quantityString.toFloatOrNull()
            if (parsedQuantity == null)
                quantity.error = requireContext().getString(R.string.wrong_number)
            else if (parsedQuantity == 0.0f)
                quantity.error = requireContext().getString(R.string.cannot_be_zero)
        } else
            quantity.error = requireContext().getString(R.string.cannot_be_empty)

        val unitString: String = unit.editText?.text.toString()
        if (unitString.isBlank())
            unit.error = requireContext().getString(R.string.cannot_be_empty)

        return ingredient.error.isNullOrBlank() && quantity.error.isNullOrBlank() && unit.error.isNullOrBlank()
    }

    override fun onDestroyView() {
        dialogView = null
        super.onDestroyView()
    }
}
