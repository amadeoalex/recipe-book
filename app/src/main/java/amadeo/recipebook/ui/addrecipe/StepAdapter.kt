package amadeo.recipebook.ui.addrecipe

import amadeo.recipebook.R
import amadeo.recipebook.recipe.Step
import amadeo.recipebook.utils.applySmoothAnimationChange
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_new_step.view.*


class StepAdapter(onItemClickListener: OnStepClickListener?) : ListAdapter<Step, StepAdapter.ComponentHolder>(ComponentDiffUtil()) {

    interface OnStepClickListener {
        fun stepClicked(step: Step)
    }

    private val itemClickListener = View.OnClickListener { view ->
        val item = view.tag as Step
        onItemClickListener?.stepClicked(item)
        (view.parent as View).applySmoothAnimationChange(view)
    }

    inner class ComponentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val content: TextView = itemView.content
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_new_step, parent, false)

        return ComponentHolder(itemView)
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        val step = getItem(position)
        holder.content.text = step.content

        with(holder.itemView) {
            tag = step
            setOnClickListener(itemClickListener)
        }
    }

    private class ComponentDiffUtil : DiffUtil.ItemCallback<Step>() {
        override fun areItemsTheSame(oldItem: Step, newItem: Step): Boolean {
            return oldItem.content == newItem.content
        }

        override fun areContentsTheSame(oldItem: Step, newItem: Step): Boolean {
            return true
        }

    }
}