package amadeo.recipebook.ui.addrecipe

import amadeo.recipebook.R
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.recipe.Step
import amadeo.recipebook.ui.recipes.RecipesViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_add_recipe.view.*

class AddRecipeFragment : Fragment(), ComponentAdapter.OnComponentClickListener, StepAdapter.OnStepClickListener, ComponentPickerDialogFragment.OnComponentPickedListener,
    StepEditorDialogFragment.OnStepAddedListener {

    private lateinit var viewModel: AddRecipeViewModel
    private lateinit var recipesViewModel: RecipesViewModel
    private lateinit var name: TextInputLayout
    private lateinit var components: RecyclerView
    private lateinit var steps: RecyclerView

    private lateinit var componentsAdapter: ComponentAdapter
    private lateinit var stepsAdapter: StepAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add_recipe, container, false)

        name = view.recipeName

        components = view.recipeComponents
        components.layoutManager = LinearLayoutManager(context)
        componentsAdapter = ComponentAdapter(this)
        components.adapter = componentsAdapter

        steps = view.recipeSteps
        steps.layoutManager = LinearLayoutManager(context)
        stepsAdapter = StepAdapter(this)
        steps.adapter = stepsAdapter

        view.addComponent.setOnClickListener {
            val fragmentManager = fragmentManager
            if (fragmentManager != null) {
                val componentPickerDialogFragment = ComponentPickerDialogFragment()
                componentPickerDialogFragment.listener = this
                componentPickerDialogFragment.show(fragmentManager, null)
            }
        }

        view.addStep.setOnClickListener {
            val fragmentManager = fragmentManager
            if (fragmentManager != null) {
                val stepEditorDialogFragment = StepEditorDialogFragment()
                stepEditorDialogFragment.listener = this
                stepEditorDialogFragment.show(fragmentManager, null)
            }
        }

        view.addRecipe.setOnClickListener {
            val nameString = name.editText?.text.toString().trim()
            if (nameString.isBlank()) {
                name.error = getString(R.string.cannot_be_empty)
            } else {
                viewModel.addRecipe(nameString)
                Navigation.findNavController(view).navigateUp()
            }
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddRecipeViewModel::class.java)
        recipesViewModel = ViewModelProviders.of(requireActivity()).get(RecipesViewModel::class.java)

        viewModel.recipeComponents.observe(viewLifecycleOwner, Observer<List<Component>> { newComponents ->
            val componentAdapter = components.adapter as ComponentAdapter
            componentAdapter.submitList(ArrayList(newComponents))
        })

        viewModel.recipeSteps.observe(viewLifecycleOwner, Observer<List<Step>> { newSteps ->
            val stepsAdapter = steps.adapter as StepAdapter
            stepsAdapter.submitList(ArrayList(newSteps))
        })
    }

    override fun componentClicked(component: Component) {
        viewModel.removeRecipeComponent(component)
    }

    override fun stepClicked(step: Step) {
        viewModel.removeRecipeStep(step)
    }

    override fun onComponentPicked(ingredient: String, quantity: Float, unit: String) {
        viewModel.addRecipeComponent(ingredient, quantity, unit)
    }

    override fun onStepAdded(step: String) {
        viewModel.addRecipeStep(step.trim())
    }

}
