package amadeo.recipebook.ui.addrecipe

import amadeo.recipebook.R
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.utils.applySmoothAnimationChange
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_new_component.view.*

class ComponentAdapter(onItemClickListener: OnComponentClickListener?) : ListAdapter<Component, ComponentAdapter.ComponentHolder>(ComponentDiffUtil()) {

    interface OnComponentClickListener {
        fun componentClicked(component: Component)
    }

    private val itemClickListener = View.OnClickListener { view ->
        val item = view.tag as Component
        onItemClickListener?.componentClicked(item)
        (view.parent as View).applySmoothAnimationChange(view)
    }

    inner class ComponentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.name
        val quantity: TextView = itemView.quantity
        val unit: TextView = itemView.unit
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_new_component, parent, false)

        return ComponentHolder(itemView)
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        val component = getItem(position)
        holder.name.text = component.ingredient.name
        holder.quantity.text = component.quantity.toString()
        holder.unit.text = component.unit.toString()

        with(holder.itemView) {
            tag = component
            setOnClickListener(itemClickListener)
        }
    }


    private class ComponentDiffUtil : DiffUtil.ItemCallback<Component>() {
        override fun areItemsTheSame(oldItem: Component, newItem: Component): Boolean {
            return oldItem.ingredient == newItem.ingredient
        }

        override fun areContentsTheSame(oldItem: Component, newItem: Component): Boolean {
            return oldItem.quantity == newItem.quantity &&
                    oldItem.unit == newItem.unit

        }

    }
}