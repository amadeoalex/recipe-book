package amadeo.recipebook.ui.addrecipe


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import amadeo.recipebook.R
import amadeo.recipebook.utils.hideKeyboard
import android.annotation.SuppressLint
import android.app.Dialog
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_step_editor_dialog.view.*


class StepEditorDialogFragment : DialogFragment() {

    interface OnStepAddedListener {
        fun onStepAdded(step: String)
    }

    var listener: OnStepAddedListener? = null

    private var dialogView: View? = null

    private lateinit var step: TextInputLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        return dialogView
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = activity!!.layoutInflater.inflate(R.layout.fragment_step_editor_dialog, null)
        dialogView = view

        step = view.step

        return MaterialAlertDialogBuilder(requireActivity())
            .setTitle(R.string.add_recipe_step)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val stepString: String = step.editText?.text.toString()
                listener?.onStepAdded(stepString)
                activity?.hideKeyboard()
            }
            .setNegativeButton(R.string.cancel) { _, _ ->
                activity?.hideKeyboard()
            }
            .setView(view)
            .create()
    }

    override fun onDestroyView() {
        dialogView = null
        super.onDestroyView()
    }
}
