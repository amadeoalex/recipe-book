package amadeo.recipebook.ui.addrecipe

import amadeo.recipebook.database.IngredientDatabase
import amadeo.recipebook.database.IngredientUnitDatabase
import amadeo.recipebook.database.RecipeDatabase
import amadeo.recipebook.entity.Ingredient
import amadeo.recipebook.entity.IngredientUnit
import amadeo.recipebook.entity.Recipe
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.recipe.Step
import amadeo.recipebook.repository.IngredientRepository
import amadeo.recipebook.repository.IngredientUnitRepository
import amadeo.recipebook.repository.RecipeRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddRecipeViewModel(application: Application) : AndroidViewModel(application) {

    val recipeComponents: MutableLiveData<ArrayList<Component>> = MutableLiveData<ArrayList<Component>>().apply {
        value = ArrayList()
    }
    val recipeSteps: MutableLiveData<ArrayList<Step>> = MutableLiveData<ArrayList<Step>>().apply {
        value = ArrayList()
    }

    private val ingredientRepository: IngredientRepository
    val ingredients: LiveData<List<Ingredient>>

    private val ingredientUnitRepository: IngredientUnitRepository
    val ingredientUnits: LiveData<List<IngredientUnit>>

    private val recipeRepository: RecipeRepository
    val recipes: LiveData<List<Recipe>>

    init {
        val ingredientDao = IngredientDatabase.getDatabase(application).ingredientDao()
        ingredientRepository = IngredientRepository(ingredientDao)
        ingredients = ingredientRepository.ingredients

        val ingredientUnitDao = IngredientUnitDatabase.getDatabase(application).ingredientUnitDao()
        ingredientUnitRepository = IngredientUnitRepository(ingredientUnitDao)
        ingredientUnits = ingredientUnitRepository.ingredientUnits

        val recipeDao = RecipeDatabase.getDatabase(application).recipeDao()
        recipeRepository = RecipeRepository(recipeDao)
        recipes = recipeRepository.recipes
    }

    fun addRecipeStep(step: String) {
        if (recipeSteps.value?.add(Step(step)) == true)
            recipeSteps.value = recipeSteps.value
    }

    fun removeRecipeStep(step: Step) {
        if (recipeSteps.value?.remove(step) == true)
            recipeSteps.value = recipeSteps.value
    }

    fun addRecipeComponent(ingredientName: String, quantity: Float, unitName: String) {
        viewModelScope.launch {
            val ingredient = newIngredient(ingredientName)
            val unit = newIngredientUnit(unitName)
            val component = Component(ingredient, quantity, unit)

            if (recipeComponents.value?.add(component) == true)
                recipeComponents.value = recipeComponents.value
        }
    }

    fun removeRecipeComponent(component: Component) {
        if (recipeComponents.value?.remove(component) == true)
            recipeComponents.value = recipeComponents.value
    }

    private suspend fun newIngredient(ingredientName: String): Ingredient {
        return ingredients.value?.firstOrNull { it.name == ingredientName }
            ?: withContext(Dispatchers.IO) {
                val newIngredient = Ingredient(0, ingredientName)
                val newId = ingredientRepository.insert(newIngredient)
                newIngredient.apply {
                    id = newId
                }
            }
    }

    private suspend fun newIngredientUnit(unitName: String): IngredientUnit {
        return ingredientUnits.value?.firstOrNull { it.name == unitName }
            ?: withContext(Dispatchers.IO) {
                val newIngredientUnit = IngredientUnit(0, unitName)
                val newId = ingredientUnitRepository.insert(newIngredientUnit)
                newIngredientUnit.apply {
                    id = newId
                }
            }
    }

    fun addRecipe(name: String) {
        val recipe = Recipe(0, name, recipeComponents.value!!, recipeSteps.value!!)
        viewModelScope.launch(Dispatchers.IO) {
            recipeRepository.insert(recipe)
        }
    }
}
