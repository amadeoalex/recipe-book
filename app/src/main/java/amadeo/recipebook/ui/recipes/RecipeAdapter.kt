package amadeo.recipebook.ui.recipes

import amadeo.recipebook.R
import amadeo.recipebook.entity.Recipe
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recipe.view.*

class RecipeAdapter(onItemClickListener: OnItemClickListener?) : ListAdapter<Recipe, RecipeAdapter.RecipeHolder>(RecipeDiffUtil()) {

    interface OnItemClickListener {
        fun itemClicked(recipe: Recipe)
    }

    private val itemClickListener = View.OnClickListener { view ->
        val item = view.tag as Recipe
        onItemClickListener?.itemClicked(item)
    }

    inner class RecipeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.recipeName
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recipe, parent, false)

        return RecipeHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        val recipe = getItem(position)
        holder.name.text = recipe.name

        with(holder.itemView) {
            tag = recipe
            setOnClickListener(itemClickListener)
        }
    }


    private class RecipeDiffUtil : DiffUtil.ItemCallback<Recipe>() {
        override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.components == newItem.components &&
                    oldItem.steps == newItem.steps
        }

    }
}