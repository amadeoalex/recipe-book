package amadeo.recipebook.ui.recipes

import amadeo.recipebook.database.IngredientDatabase
import amadeo.recipebook.database.IngredientUnitDatabase
import amadeo.recipebook.database.RecipeDatabase
import amadeo.recipebook.entity.Ingredient
import amadeo.recipebook.entity.IngredientUnit
import amadeo.recipebook.entity.Recipe
import amadeo.recipebook.entity.isLabel
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.repository.IngredientRepository
import amadeo.recipebook.repository.IngredientUnitRepository
import amadeo.recipebook.repository.RecipeRepository
import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class RecipesViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: RecipeRepository
    private val filter: MutableLiveData<String> = MutableLiveData("")
    val recipes: LiveData<List<Recipe>>

    private val ingredientRepository: IngredientRepository
    private val ingredients: LiveData<List<Ingredient>>

    private val ingredientUnitRepository: IngredientUnitRepository
    private val ingredientUnits: LiveData<List<IngredientUnit>>

    init {
        val recipeDao = RecipeDatabase.getDatabase(application).recipeDao()
        repository = RecipeRepository(recipeDao)
        recipes = Transformations.switchMap(filter, ::getRecipesWithName)

        val ingredientDao = IngredientDatabase.getDatabase(application).ingredientDao()
        ingredientRepository = IngredientRepository(ingredientDao)
        ingredients = ingredientRepository.ingredients

        val ingredientUnitDao = IngredientUnitDatabase.getDatabase(application).ingredientUnitDao()
        ingredientUnitRepository = IngredientUnitRepository(ingredientUnitDao)
        ingredientUnits = ingredientUnitRepository.ingredientUnits
    }

    private fun getRecipesWithName(name: String?): LiveData<List<Recipe>> {
        return if (name.isNullOrBlank())
            repository.recipes
        else
            repository.getAllWithName(name)
    }

    fun filterRecipes(newFilter: String?) {
        filter.postValue(newFilter)
    }

    fun importRecipes(file: File) {
        val fileContent = readFileLines(file)

        viewModelScope.launch(Dispatchers.IO) {
            importRecipes(fileContent)
        }
    }

    private suspend fun importRecipes(fileContent: String) {
        val recipes: List<Recipe> = Json.parse(Recipe.serializer().list, fileContent)
        for (recipe in recipes) {
            val componentsIterator: MutableListIterator<Component> = (recipe.components as ArrayList).listIterator()
            while (componentsIterator.hasNext()) {
                val currentComponent = componentsIterator.next()

                val ingredient = parseIngredient(currentComponent.ingredient.name)
                val unit = parseIngredientUnit(currentComponent.unit.name)
                val newComponent = Component(ingredient, currentComponent.quantity, unit)

                componentsIterator.set(newComponent)
            }

            recipe.id = 0
        }
        repository.insertAll(recipes)
    }

    private fun readFileLines(file: File): String {
        val bufferedReader = BufferedReader(InputStreamReader(file.inputStream()))
        bufferedReader.use { reader ->
            val stringBuilder = StringBuilder()
            while (true) {
                val line = reader.readLine() ?: break
                stringBuilder.append(line)
            }
            return stringBuilder.toString()
        }
    }

    private suspend fun parseIngredient(ingredientName: String): Ingredient {
        return ingredients.value?.firstOrNull { it.name == ingredientName }
            ?: withContext(Dispatchers.IO) {
                val newIngredient = Ingredient(0, ingredientName)
                if (!newIngredient.isLabel()) {
                    val newId = ingredientRepository.insert(newIngredient)
                    newIngredient.apply {
                        id = newId
                    }
                }
                newIngredient
            }
    }

    private suspend fun parseIngredientUnit(unitName: String): IngredientUnit {
        return ingredientUnits.value?.firstOrNull { it.name == unitName }
            ?: withContext(Dispatchers.IO) {
                val newIngredientUnit = IngredientUnit(0, unitName)
                if (!newIngredientUnit.isLabel()) {
                    val newId = ingredientUnitRepository.insert(newIngredientUnit)
                    newIngredientUnit.apply {
                        id = newId
                    }
                }
                newIngredientUnit
            }
    }
}
