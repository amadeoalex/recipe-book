package amadeo.recipebook.ui.recipes

import amadeo.recipebook.R
import amadeo.recipebook.entity.Recipe
import amadeo.recipebook.ui.recipe.RecipeFragment
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_recipes.view.*
import java.io.File

class RecipesFragment : Fragment(), RecipeAdapter.OnItemClickListener {

    companion object {
        private const val CHOOSE_FILE_REQUEST = 123
    }

    private lateinit var viewModel: RecipesViewModel

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recipes, container, false)

        recyclerView = view.recipes
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = RecipeAdapter(this)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(RecipesViewModel::class.java)
        viewModel.recipes.observe(this, Observer<List<Recipe>> { recipes ->
            val adapter = recyclerView.adapter as RecipeAdapter
            adapter.submitList(recipes)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.recipes, menu)

        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.queryHint = getString(R.string.search_recipe)
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("RecipeBook", "$newText")
                viewModel.filterRecipes(newText)
                return true
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_import -> {
                val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                    type = "file/*"
                }

                startActivityForResult(intent, CHOOSE_FILE_REQUEST)
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CHOOSE_FILE_REQUEST -> {
                if (data == null)
                    return

                val uri: Uri = data.data ?: return
                val path = uri.path ?: return
                val file = File(path)

                viewModel.importRecipes(file)
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun itemClicked(recipe: Recipe) {
        Snackbar.make(view!!, "Clicked on " + recipe.name, Snackbar.LENGTH_SHORT).show()
        Navigation.findNavController(view!!).navigate(R.id.action_show_recipe, RecipeFragment.createBundle(recipe.id, recipe.name))
    }
}
