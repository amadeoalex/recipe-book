package amadeo.recipebook.ui.recipe

import amadeo.recipebook.R
import amadeo.recipebook.recipe.Step
import amadeo.recipebook.recipe.getLabel
import amadeo.recipebook.recipe.isLabel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_label.view.*
import kotlinx.android.synthetic.main.item_step.view.*

class StepAdapter(onItemClickListener: OnStepClickListener?) : ListAdapter<Step, RecyclerView.ViewHolder>(ComponentDiffUtil()) {

    companion object {
        private const val ITEM_TYPE_STEP = 0
        private const val ITEM_TYPE_LABEL = 1
    }

    interface OnStepClickListener {
        fun stepClicked(step: Step)
    }

    private val itemClickListener = View.OnClickListener { view ->
        val item = view.tag as Step
        onItemClickListener?.stepClicked(item)
    }

    inner class StepHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val content: TextView = itemView.content
    }

    inner class LabelHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label: TextView = itemView.label
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item.isLabel()) ITEM_TYPE_LABEL else ITEM_TYPE_STEP
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_TYPE_LABEL) {
            LabelHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_label, parent, false)
            )
        } else {
            StepHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_step, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val step = getItem(position)

        if (step.isLabel()) {
            holder as LabelHolder
            holder.label.text = step.getLabel()
        } else {
            holder as StepHolder
            holder.content.text = step.content

            with(holder.itemView) {
                tag = step
                setOnClickListener(itemClickListener)
            }
        }
    }

    private class ComponentDiffUtil : DiffUtil.ItemCallback<Step>() {
        override fun areItemsTheSame(oldItem: Step, newItem: Step): Boolean {
            return oldItem.content == newItem.content
        }

        override fun areContentsTheSame(oldItem: Step, newItem: Step): Boolean {
            return true
        }
    }
}