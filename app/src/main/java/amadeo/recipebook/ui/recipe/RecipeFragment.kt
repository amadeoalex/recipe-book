package amadeo.recipebook.ui.recipe


import amadeo.recipebook.R
import amadeo.recipebook.entity.Recipe
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_recipe.view.*

class RecipeFragment : Fragment() {

    companion object {
        private const val ARG_RECIPE_ID = "recipeId"
        private const val ARG_RECIPE_NAME = "recipeName"

        fun createBundle(recipeId: Long, recipeName: String): Bundle {
            return Bundle().apply {
                putLong(ARG_RECIPE_ID, recipeId)
                putString(ARG_RECIPE_NAME, recipeName)
            }
        }
    }

    private var recipeId: Long = -1

    private lateinit var name: TextView
    private lateinit var components: RecyclerView
    private lateinit var steps: RecyclerView
    private lateinit var ingredientsTextView: TextView
    private lateinit var stepsTextView: TextView

    private lateinit var viewModel: RecipeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val args = arguments

        require(args != null) { "missing arguments" }
        require(args.containsKey(ARG_RECIPE_ID)) { "arguments are missing recipe id - pass bundle from createBundle() function" }

        recipeId = args.getLong(ARG_RECIPE_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recipe, container, false)

        name = view.recipeName

        components = view.recipeComponents
        components.isNestedScrollingEnabled = false
        val componentsLayoutManger = LinearLayoutManager(context)
        components.layoutManager = componentsLayoutManger
        components.addItemDecoration(DividerItemDecoration(context, componentsLayoutManger.orientation))
        components.adapter = ComponentAdapter(null)

        steps = view.recipeSteps
        components.isNestedScrollingEnabled = false
        val stepsLayoutManager = LinearLayoutManager(context)
        steps.layoutManager = stepsLayoutManager
        steps.addItemDecoration(DividerItemDecoration(context, stepsLayoutManager.orientation))
        steps.adapter = StepAdapter(null)

        ingredientsTextView = view.ingredientsTextView
        stepsTextView = view.stepsTextView

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this)[RecipeViewModel::class.java]
        viewModel.getRecipe(recipeId)
        viewModel.recipe.observe(viewLifecycleOwner, Observer<Recipe> { recipe ->
            if (recipe != null && recipe.id == recipeId) {
                name.text = recipe.name
                ingredientsTextView.visibility = View.VISIBLE
                stepsTextView.visibility = View.VISIBLE

                (this.components.adapter as ComponentAdapter).submitList(recipe.components)
                (this.steps.adapter as StepAdapter).submitList(recipe.steps)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.recipe, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_remove_recipe -> {
                viewModel.removeRecipe(recipeId)
                Navigation.findNavController(view!!).navigateUp()
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
