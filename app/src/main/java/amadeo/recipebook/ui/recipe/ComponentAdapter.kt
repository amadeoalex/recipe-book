package amadeo.recipebook.ui.recipe

import amadeo.recipebook.R
import amadeo.recipebook.recipe.Component
import amadeo.recipebook.recipe.getLabel
import amadeo.recipebook.recipe.isLabel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_component.view.*
import kotlinx.android.synthetic.main.item_label.view.*

class ComponentAdapter(onItemClickListener: OnComponentClickListener?) : ListAdapter<Component, RecyclerView.ViewHolder>(
    ComponentDiffUtil()
) {

    companion object {
        private const val ITEM_TYPE_COMPONENT = 0
        private const val ITEM_TYPE_LABEL = 1
    }

    interface OnComponentClickListener {
        fun componentClicked(component: Component)
    }

    private val itemClickListener = View.OnClickListener { view ->
        val item = view.tag as Component
        onItemClickListener?.componentClicked(item)
    }

    inner class ComponentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.name
        val quantity: TextView = itemView.quantity
        val unit: TextView = itemView.unit
    }

    inner class LabelHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label: TextView = itemView.label
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item.isLabel()) ITEM_TYPE_LABEL else ITEM_TYPE_COMPONENT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_TYPE_LABEL) {
            LabelHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_label, parent, false)
            )
        } else {
            ComponentHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_component, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val component = getItem(position)

        if (component.isLabel()) {
            holder as LabelHolder
            holder.label.text = component.getLabel()
        } else {
            holder as ComponentHolder
            holder.name.text = component.ingredient.name
            holder.quantity.text = component.quantity.toString()
            holder.unit.text = component.unit.toString()

            with(holder.itemView) {
                tag = component
                setOnClickListener(itemClickListener)
            }
        }
    }


    private class ComponentDiffUtil : DiffUtil.ItemCallback<Component>() {
        override fun areItemsTheSame(oldItem: Component, newItem: Component): Boolean {
            return oldItem.ingredient == newItem.ingredient
        }

        override fun areContentsTheSame(oldItem: Component, newItem: Component): Boolean {
            return oldItem.quantity == newItem.quantity &&
                    oldItem.unit == newItem.unit

        }
    }
}