package amadeo.recipebook.ui.recipe

import amadeo.recipebook.database.RecipeDatabase
import amadeo.recipebook.entity.Recipe
import amadeo.recipebook.repository.RecipeRepository
import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecipeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: RecipeRepository
    private val recipeId: MutableLiveData<Long> = MutableLiveData()
    val recipe: LiveData<Recipe>

    init {
        val recipeDao = RecipeDatabase.getDatabase(application).recipeDao()
        repository = RecipeRepository(recipeDao)

        recipe = Transformations.switchMap(recipeId, ::getRecipeWithId)
    }

    private fun getRecipeWithId(recipeId: Long): LiveData<Recipe> {
        return repository.getRecipe(recipeId)
    }

    fun getRecipe(recipeId: Long) {
        this.recipeId.postValue(recipeId)
    }

    fun removeRecipe(recipeId: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.delete(recipeId)
        }
    }
}
