# Recipe Book

This is simple digital version of recipe book designed and created for my mom. Throughout the years she used many hand written recipe books which "dissolved" after many years of usage.

* It is almost 2020 - everything is getting digitalized
* She has a programmer in the house
* I have and old "noname" Modecom tablet laying around

It is the the time to fix the recipe book issue once and for all ;)

## Project status

 - [x] In early development

## Installation

For now you will have to download the sources and build the app yourself.

## Built With

* [Android Studio](https://developer.android.com/studio/)
* [Gradle](https://gradle.org/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details